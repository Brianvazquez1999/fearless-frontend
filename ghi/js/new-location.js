function createState(name, abbreviation) {
    return `
    <option value=${abbreviation}> ${name}</option>`
}


window.addEventListener('DOMContentLoaded', async () => {
    const stateUrl = "http://localhost:8000/api/states/."
    const response = await fetch(stateUrl)
    if (response.ok) {
        const data = await response.json()
        for (const state of data.states) {
            const name = state.name
            const abbreviation= state.abbreviation
            const html = createState(name, abbreviation)
            const select = document.querySelector("#state")
            select.innerHTML += html


        }
    }
})
window.addEventListener('DOMContentLoaded', async () => {

    // State fetching code, here...

    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit', async event => {
      event.preventDefault();
      const formData = new FormData(formTag);
const json = JSON.stringify(Object.fromEntries(formData));


const locationUrl = 'http://localhost:8000/api/locations/';
const fetchConfig = {
  method: "post",
  body: json,
  headers: {
    'Content-Type': 'application/json',
  },
};
const response = await fetch(locationUrl, fetchConfig);
if (response.ok) {
  formTag.reset();
  const newLocation = await response.json();

}
    });
  });
