
function createConferece(name, id) {
    return `
    <option value=${id}>${name}</option>`
}
window.addEventListener("DOMContentLoaded", async () => {
    const locationUrl = "http://localhost:8000/api/locations/"
    const response = await fetch(locationUrl)
    if (response.ok) {
        const data = await response.json()
        for (const conference of data.locations) {
            const name = conference.name
            const id = conference.id
            const html = createConferece(name, id)
            const select = document.querySelector("#location")
            select.innerHTML += html

        }
    }
})

window.addEventListener("DOMContentLoaded",async () => {
    const formTag = document.querySelector("#create-conference-form")
    formTag.addEventListener("submit", async  event => {
        event.preventDefault()
        const formData = new FormData(formTag)
        const json = JSON.stringify(Object.fromEntries(formData))
        const url = "http://localhost:8000/api/conferences/"
        const fetchConfig = {
            method:"post",
            body: json,
            headers: {
                'Content-Type': 'application/json',

            },
        };
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            formTag.reset();
            const newConference = await response.json()

        }

    })
} )
