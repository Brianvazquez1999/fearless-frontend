function createCard(name,location ,startDate, endDate, description, pictureUrl) {
    return`
    <div class="col">
    <div class="shadow p-3 mb-5 bg-body-tertiary rounded">
      <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>

        </div>
        <div class="card-footer">${startDate} - ${endDate}</div>
      </div>
      </div>
      </div>

    `;
  }



window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';


    try {
        const response = await fetch(url);

        if (!response.ok) {
          // Figure out what to do when the response is bad
        } else {
          const data = await response.json();
        let count = 0
          for(let conference of data.conferences) {
            count ++
            if (count === 4) {
                count = 1
            }
            console.log(conference)
            const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl)
          if (detailResponse.ok) {
            const details = await detailResponse.json()
            const start = new Date(details.conference.starts)
            const end = new Date(details.conference.ends)
            const endDate = end.toLocaleDateString()
            const startDate = start.toLocaleDateString()
            const location = details.conference.location.name
            const name = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const html = createCard(name,location,startDate, endDate, description, pictureUrl);
            const column = document.querySelector(`#col-${count}`);
                column.innerHTML += html;



          }

          }
        }
    } catch (e) {
        alert(console.log(e))
      }
});
